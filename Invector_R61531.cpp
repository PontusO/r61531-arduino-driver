/*!
 * @file Invector_R61531.cpp
 *
 * @mainpage Generic R61531 TFT Displays
 *
 * @section intro_sec Introduction
 *
 *
 * This library depends on <a href="https://github.com/adafruit/Adafruit_GFX">
 * Adafruit_GFX</a> being present on your system. Please make sure you have
 * installed the latest version before using this library.
 *
 * @section author Author
 *
 * Written by Limor "ladyada" Fried for Adafruit Industries.
 * Modifications for R61531 displays made by Pontus Oldberg, Invector Labs
 *
 * @section license License
 *
 * BSD license, all text here must be included in any redistribution.
 *
 */

#include "Invector_R61531.h"
#ifndef ARDUINO_STM32_FEATHER
#include "pins_arduino.h"
#ifndef RASPI
#include "wiring_private.h"
#endif
#endif
#include <limits.h>

#if defined(ARDUINO_ARCH_ARC32) || defined(ARDUINO_MAXIM)
#define SPI_DEFAULT_FREQ 16000000
// Teensy 3.0, 3.1/3.2, 3.5, 3.6
#elif defined(__MK20DX128__) || defined(__MK20DX256__) ||                      \
    defined(__MK64FX512__) || defined(__MK66FX1M0__)
#define SPI_DEFAULT_FREQ 40000000
#elif defined(__AVR__) || defined(TEENSYDUINO)
#define SPI_DEFAULT_FREQ 8000000
#elif defined(ESP8266) || defined(ESP32)
#define SPI_DEFAULT_FREQ 40000000
#elif defined(RASPI)
#define SPI_DEFAULT_FREQ 80000000
#elif defined(ARDUINO_ARCH_STM32F1)
#define SPI_DEFAULT_FREQ 36000000
#else
#define SPI_DEFAULT_FREQ 24000000 ///< Default SPI data clock frequency
#endif

#define MADCTL_MY   0x80  ///< Bottom to top
#define MADCTL_MX   0x40  ///< Right to left
#define MADCTL_MV   0x20  ///< Reverse Mode
#define MADCTL_ML   0x10  ///< LCD refresh Bottom to top
#define MADCTL_RGB  0x00  ///< Red-Green-Blue pixel order
#define MADCTL_BGR  0x08  ///< Blue-Green-Red pixel order
#define MADCTL_MH   0x04  ///< LCD refresh right to left
#define MADCTL_FP   0x01  ///< Flip screen

/**************************************************************************/
/*!
    @brief  Instantiate Generic R61531 driver with software SPI
    @param    cs    Chip select pin #
    @param    dc    Data/Command pin #
    @param    mosi  SPI MOSI pin #
    @param    sclk  SPI Clock pin #
    @param    rst   Reset pin # (optional, pass -1 if unused)
    @param    miso  SPI MISO pin # (optional, pass -1 if unused)
*/
/**************************************************************************/
Invector_R61531::Invector_R61531(int8_t cs, int8_t dc, int8_t mosi,
                                   int8_t sclk, int8_t rst, int8_t miso)
    : Invector_SPITFT(R61531_TFTWIDTH, R61531_TFTHEIGHT, cs, dc, mosi, sclk,
                      rst, miso) {}

/**************************************************************************/
/*!
    @brief  Instantiate generic R61531 driver with hardware SPI using the
            default SPI peripheral.
    @param  cs   Chip select pin # (OK to pass -1 if CS tied to GND).
    @param  dc   Data/Command pin # (required).
    @param  rst  Reset pin # (optional, pass -1 if unused).
*/
/**************************************************************************/
Invector_R61531::Invector_R61531(int8_t cs, int8_t dc, int8_t rst)
    : Invector_SPITFT(R61531_TFTWIDTH, R61531_TFTHEIGHT, cs, dc, rst) {}

#if !defined(ESP8266)
/**************************************************************************/
/*!
    @brief  Instantiate generic R61531 driver with hardware SPI using
            a specific SPI peripheral (not necessarily default).
    @param  spiClass  Pointer to SPI peripheral (e.g. &SPI or &SPI1).
    @param  dc        Data/Command pin # (required).
    @param  cs        Chip select pin # (optional, pass -1 if unused and
                      CS is tied to GND).
    @param  rst       Reset pin # (optional, pass -1 if unused).
*/
/**************************************************************************/
Invector_R61531::Invector_R61531(SPIClass *spiClass, int8_t dc, int8_t cs,
                                   int8_t rst)
    : Invector_SPITFT(R61531_TFTWIDTH, R61531_TFTHEIGHT, spiClass, cs, dc,
                      rst) {}
#endif // end !ESP8266

/**************************************************************************/
/*!
    @brief  Instantiate generic R61531 driver using parallel interface.
    @param  busWidth  If tft16 (enumeration in Adafruit_SPITFT.h), is a
                      16-bit interface, else 8-bit.
    @param  d0        Data pin 0 (MUST be a byte- or word-aligned LSB of a
                      PORT register -- pins 1-n are extrapolated from this).
    @param  wr        Write strobe pin # (required).
    @param  dc        Data/Command pin # (required).
    @param  cs        Chip select pin # (optional, pass -1 if unused and CS
                      is tied to GND).
    @param  rst       Reset pin # (optional, pass -1 if unused).
    @param  rd        Read strobe pin # (optional, pass -1 if unused).
*/
/**************************************************************************/
Invector_R61531::Invector_R61531(tftBusWidth busWidth, int8_t d0, int8_t wr,
                                   int8_t dc, int8_t cs, int8_t rst, int8_t rd)
    : Invector_SPITFT(R61531_TFTWIDTH, R61531_TFTHEIGHT, busWidth, d0, wr, dc,
                      cs, rst, rd) {}

// clang-format off
static const uint8_t PROGMEM initcmd[] = {
  R61531_UNLOCK, 1, 0x04,
  R61531_MADCTL, 1, 0x08,
  R61531_PIXFMT, 1, 0x05,
  R61531_TEARON, 1, 0x00,
  R61531_PNLDS,  7, 0x01, 0x3f, 0x40, 0x10, 0x00, 0x01, 0x00,
  R61531_DTSNM,  5, 0x07, 0x28, 0x32, 0x09, 0x00,
  R61531_SGDTS,  4, 0x21, 0x00, 0x04, 0x02,
  R61531_GAMSA, 24, 0x04, 0x0d, 0x10, 0x16, 0x1e, 0x30, 0x59, 0x4c, 0x40, 0x35,
                    0x26, 0x1a, 0x04, 0x0d, 0x10, 0x16, 0x1e, 0x30, 0x59, 0x4c,
                    0x40, 0x35, 0x26, 0x1a,
  R61531_GAMSB, 24, 0x0c, 0x0f, 0x13, 0x18, 0x22, 0x34, 0x58, 0x48, 0x38, 0x2b,
                    0x1e, 0x1a, 0x0c, 0x0e, 0x13, 0x18, 0x22, 0x34, 0x57, 0x48,
                    0x38, 0x2b, 0x1e, 0x1a,
  R61531_GAMSC, 24, 0x04, 0x0f, 0x14, 0x1f, 0x26, 0x37, 0x56, 0x46, 0x3a, 0x32,
                    0x2d, 0x1a, 0x04, 0x0f, 0x14, 0x1f, 0x26, 0x37, 0x56, 0x46,
                    0x3a, 0x32, 0x2d, 0x1a,
  R61531_PWRSET,16, 0x29, 0x0c, 0x0e, 0x20, 0x17, 0x04, 0x01, 0x00, 0x08, 0x01,
                    0x00, 0x06, 0x01, 0x00, 0x00, 0x20,
  R61531_VCOM,   4, 0x02, 0x42, 0x35, 0x69,
  R61531_WRDDB,  6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  R61531_NVLC,   1, 0x80,
  R61531_CASET,  4, 0x00, 0x00, 0x01, 0xdf,
  R61531_PASET,  4, 0x00, 0x00, 0x01, 0x3f,
  R61531_SLPOUT, 0,
  R61531_DLY,    1, 130,
  R61531_DISPON, 0,
  R61531_EOT                                   // End of list
};
// clang-format on

/**************************************************************************/
/*!
    @brief   Initialize R61531 chip
    Connects to the R61531 over SPI and sends initialization procedure commands
    @param    freq  Desired SPI clock frequency
*/
/**************************************************************************/
void Invector_R61531::begin(uint32_t freq) {

  if (!freq)
    freq = SPI_DEFAULT_FREQ;

  initSPI(freq);

  if (_rst < 0) {                 // If no hardware reset pin...
    sendCommand(R61531_SWRESET); // Engage software reset
    delay(150);
  }

  uint8_t cmd, x, numArgs;
  const uint8_t *addr = initcmd;
  while ((cmd = pgm_read_byte(addr++)) != R61531_EOT) {
    x = pgm_read_byte(addr++);
    numArgs = x & 0x7F;
    if (cmd == R61531_DLY) {
      delay((int)pgm_read_byte(addr++));
    } else {
      sendCommand(cmd, addr, numArgs);
      addr += numArgs;
    }
  }

  _width = R61531_TFTWIDTH;
  _height = R61531_TFTHEIGHT;
}

/**************************************************************************/
/*!
    @brief   Set origin of (0,0) and orientation of TFT display
    @param   m  The index for rotation, from 0-3 inclusive
*/
/**************************************************************************/
void Invector_R61531::setRotation(uint8_t m) {
  rotation = m % 4; // can't be higher than 3
  switch (rotation) {
  case 0:
    m = (MADCTL_BGR);
    _width = R61531_TFTWIDTH;
    _height = R61531_TFTHEIGHT;
    break;
  case 1:
    m = (MADCTL_MY |  MADCTL_MV | MADCTL_BGR);  // TODO: Still is wonky
    _width = R61531_TFTHEIGHT;
    _height = R61531_TFTWIDTH;
    break;
  case 2:
    m = (MADCTL_FP | MADCTL_MX | MADCTL_BGR);
    _width = R61531_TFTWIDTH;
    _height = R61531_TFTHEIGHT;
    break;
  case 3:
    m = (MADCTL_MX |  MADCTL_MV | MADCTL_BGR);
    _width = R61531_TFTHEIGHT;
    _height = R61531_TFTWIDTH;
    break;
  }

  sendCommand(R61531_MADCTL, &m, 1);
}

/**************************************************************************/
/*!
    @brief   Enable/Disable display color inversion
    @param   invert True to invert, False to have normal color
*/
/**************************************************************************/
void Invector_R61531::invertDisplay(bool invert) {
  sendCommand(invert ? R61531_INVON : R61531_INVOFF);
}

#if defined(SCROLL_SUPPORT)   // R61531 does not seem to have scroll support
/**************************************************************************/
/*!
    @brief   Scroll display memory
    @param   y How many pixels to scroll display by
*/
/**************************************************************************/
void Invector_R61531::scrollTo(uint16_t y) {
  uint8_t data[2];
  data[0] = y >> 8;
  data[1] = y & 0xff;
  sendCommand(R61531_VSCRSADD, (uint8_t *)data, 2);
}

/**************************************************************************/
/*!
    @brief   Set the height of the Top and Bottom Scroll Margins
    @param   top The height of the Top scroll margin
    @param   bottom The height of the Bottom scroll margin
 */
/**************************************************************************/
void Invector_R61531::setScrollMargins(uint16_t top, uint16_t bottom) {
  // TFA+VSA+BFA must equal 320
  if (top + bottom <= R61531_TFTHEIGHT) {
    uint16_t middle = R61531_TFTHEIGHT - top + bottom;
    uint8_t data[6];
    data[0] = top >> 8;
    data[1] = top & 0xff;
    data[2] = middle >> 8;
    data[3] = middle & 0xff;
    data[4] = bottom >> 8;
    data[5] = bottom & 0xff;
    sendCommand(R61531_VSCRDEF, (uint8_t *)data, 6);
  }
}
#endif

/**************************************************************************/
/*!
    @brief   Set the "address window" - the rectangle we will write to RAM with
   the next chunk of SPI data writes. The R61531 will automatically wrap
   the data as each row is filled
    @param   y1  TFT memory 'y' origin
    @param   x1  TFT memory 'x' origin
    @param   w   Width of rectangle
    @param   h   Height of rectangle
*/
/**************************************************************************/
void Invector_R61531::setAddrWindow(uint16_t x1, uint16_t y1, uint16_t w,
                                     uint16_t h) {
  uint16_t x2 = (x1 + w - 1), y2 = (y1 + h - 1);
  writeCommand(R61531_CASET); // Column address set
  SPI_WRITE16(x1);
  SPI_WRITE16(x2);
  if (_cs > 0) {
    SPI_CS_HIGH();
    SPI_CS_LOW();
  }
  writeCommand(R61531_PASET); // Row address set
  SPI_WRITE16(y1);
  SPI_WRITE16(y2);
  if (_cs > 0) {
    SPI_CS_HIGH();
    SPI_CS_LOW();
  }
  writeCommand(R61531_RAMWR); // Write to RAM
}

/**************************************************************************/
/*!
    @brief  Read 8 bits of data from R61531 configuration memory. NOT from RAM!
            This is highly undocumented/supported, it's really a hack but kinda
   works?
    @param    commandByte  The command register to read data from
    @param    index  The byte index into the command to read from
    @return   Unsigned 8-bit data read from R61531 register

    TODO: This doesn't work yet. Seems to hang the display.
 */
/**************************************************************************/
uint8_t Invector_R61531::readcommand8(uint8_t commandByte, uint8_t index) {
  uint8_t data = 0x10 + index;
  sendCommand(0xD9, &data, 1); // Set Index Register
  return Invector_SPITFT::readcommand8(commandByte);
}
