/*!
 * @file Invector_R61531.h
 *
 * This is the documentation for Invector's R61531 driver for the
 * Arduino platform.
 *
 * These displays use SPI to communicate, 4 or 5 pins are required
 * to interface (RST is optional).
 *
 *
 * This library depends on <a href="https://github.com/adafruit/Adafruit_GFX">
 * Adafruit_GFX</a> being present on your system. Please make sure you have
 * installed the latest version before using this library.
 *
 * Modified by Pontus Oldberg for Invector Industries.
 *
 * BSD license, all text here must be included in any redistribution.
 *
 */

#ifndef _INVECTOR_R61531H_
#define _INVECTOR_R61531H_

#include "Adafruit_GFX.h"
#include "Arduino.h"
#include "Print.h"
#include <Invector_SPITFT.h>
#include <SPI.h>

#define R61531_TFTWIDTH     480     ///< R61531 max TFT width
#define R61531_TFTHEIGHT    320     ///< R61531 max TFT height

#define R61531_NOP          0x00    ///< No-op register
#define R61531_SWRESET      0x01    ///< Software reset register
#define R61531_RDDDB        0x04    ///< Read display identification information
//#define R61531_RDDST        0x09  ///< Read Display Status

#define R61531_RDMODE       0x0A    ///< Read Display Power Mode
#define R61531_RDMADCTL     0x0B    ///< Read Display MADCTL
#define R61531_RDPIXFMT     0x0C    ///< Read Display Pixel Format
#define R61531_RDIMGFMT     0x0D    ///< Read Display Image Format
#define R61531_RDSELFDIAG   0x0F    ///< Read Display Self-Diagnostic Result

#define R61531_SLPIN        0x10    ///< Enter Sleep Mode
#define R61531_SLPOUT       0x11    ///< Sleep Out
#define R61531_PTLON        0x12    ///< Partial Mode ON
#define R61531_NORON        0x13    ///< Normal Display Mode ON

#define R61531_INVOFF       0x20    ///< Display Inversion OFF
#define R61531_INVON        0x21    ///< Display Inversion ON
//#define R61531_GAMMASET     0x26  ///< Gamma Set
#define R61531_DISPOFF      0x28    ///< Display OFF
#define R61531_DISPON       0x29    ///< Display ON

#define R61531_CASET        0x2A    ///< Column Address Set
#define R61531_PASET        0x2B    ///< Page Address Set
#define R61531_RAMWR        0x2C    ///< Memory Write
#define R61531_RAMRD        0x2E    ///< Memory Read

#define R61531_PTLAR        0x30    ///< Partial Area
//#define R61531_VSCRDEF      0x33  ///< Vertical Scrolling Definition
#define R61531_TEAROFF      0x34    ///< Turns off tearing effect
#define R61531_TEARON       0x35    ///< Turns on tearing effect
#define R61531_MADCTL       0x36    ///< Memory Access Control
//#define R61531_VSCRSADD     0x37  ///< Vertical Scrolling Start Address
#define R61531_PIXFMT       0x3A    ///< COLMOD: Pixel Format Set

#define R61531_WMC          0x3C    ///< Write data from previos location
#define R61531_RMC          0x3E    ///< Read data from previos location
#define R61531_TEARLN       0x44    ///< Set tearing on screen row
#define R61531_SCANLN       0x45    ///< Get current can line

#define R61531_UNLOCK       0xB0    ///< Unlocks restricted access to manufacturer registers
#define R61531_LPMC         0xB1    ///< Low power mode control
//#define R61531_FRMCTR2      0xB2  ///< Frame Rate Control (In Idle Mode/8 colors)
#define R61531_FMAIS        0xB3    ///< Frame Memory Access and Interface Setting
//#define R61531_INVCTR       0xB4    ///< Display Inversion Control
#define R61531_RCECC        0xB5    ///< Read Checksum and ECC Error Count
#define R61531_DSICTRL      0xB6    ///< DSI Control
#define R61531_BLCTRL1      0xB8    ///< Backlight control 1
#define R61531_BLCTRL2      0xB9    ///< Backlight control 2
#define R61531_BLCTRL3      0xBA    ///< Backlight control 3
#define R61531_BLCTRL4      0xBC    ///< Backlight control 4

#define R61531_DCR          0xBF    ///> Device code read

#define R61531_PNLDS        0xC0    ///< Panel driving setting
#define R61531_DTSNM        0xC1    ///< Display Timing Setting for Normal Mode
#define R61531_DSPCS        0xC3    ///< Display Control Setting
#define R61531_SGDTS        0xC4    ///< Source/Gate Driving Timing Setting
#define R61531_GAMSA        0xC8    ///< Gamma Setting A Set
#define R61531_GAMSB        0xC9    ///< Gamma Setting B Set
#define R61531_GAMSC        0xCA    ///< Gamma Setting C Set

#define R61531_PWRSET       0xD0    ///< Power Settings
#define R61531_VCOM         0xD1    ///< VCOM Settings

#define R61531_INTPWR       0xD3    ///< Internal power settings

#define R61531_NVMAC        0xE0    ///< NVM Access control
#define R61531_WRDDB        0xE1    ///< set_DDB_write_control
#define R61531_NVLC         0xE2    ///< NVM Load control
#define R61531_NVMTST1      0xE4    ///< NVM test 1

// Special table codes
#define R61531_DLY          0xFF    ///< Perfroms a delay during the loading of regs.
#define R61531_EOT          0xFE    ///< This marks the end of the init table

// Color definitions
#define R61531_BLACK 0x0000       ///<   0,   0,   0
#define R61531_NAVY 0x000F        ///<   0,   0, 123
#define R61531_DARKGREEN 0x03E0   ///<   0, 125,   0
#define R61531_DARKCYAN 0x03EF    ///<   0, 125, 123
#define R61531_MAROON 0x7800      ///< 123,   0,   0
#define R61531_PURPLE 0x780F      ///< 123,   0, 123
#define R61531_OLIVE 0x7BE0       ///< 123, 125,   0
#define R61531_LIGHTGREY 0xC618   ///< 198, 195, 198
#define R61531_DARKGREY 0x7BEF    ///< 123, 125, 123
#define R61531_BLUE 0x001F        ///<   0,   0, 255
#define R61531_GREEN 0x07E0       ///<   0, 255,   0
#define R61531_CYAN 0x07FF        ///<   0, 255, 255
#define R61531_RED 0xF800         ///< 255,   0,   0
#define R61531_MAGENTA 0xF81F     ///< 255,   0, 255
#define R61531_YELLOW 0xFFE0      ///< 255, 255,   0
#define R61531_WHITE 0xFFFF       ///< 255, 255, 255
#define R61531_ORANGE 0xFD20      ///< 255, 165,   0
#define R61531_GREENYELLOW 0xAFE5 ///< 173, 255,  41
#define R61531_PINK 0xFC18        ///< 255, 130, 198

/**************************************************************************/
/*!
@brief Class to manage hardware interface with R61531 chipset (also seems to
work with ILI9340)
*/
/**************************************************************************/

class Invector_R61531 : public Invector_SPITFT {
public:
  Invector_R61531(int8_t _CS, int8_t _DC, int8_t _MOSI, int8_t _SCLK,
                   int8_t _RST = -1, int8_t _MISO = -1);
  Invector_R61531(int8_t _CS, int8_t _DC, int8_t _RST = -1);
#if !defined(ESP8266)
  Invector_R61531(SPIClass *spiClass, int8_t dc, int8_t cs = -1,
                   int8_t rst = -1);
#endif // end !ESP8266
  Invector_R61531(tftBusWidth busWidth, int8_t d0, int8_t wr, int8_t dc,
                   int8_t cs = -1, int8_t rst = -1, int8_t rd = -1);

  void begin(uint32_t freq = 0);
  void setRotation(uint8_t r);
  void invertDisplay(bool i);
#if defined(SCROLL_SUPPORT)
  void scrollTo(uint16_t y);
  void setScrollMargins(uint16_t top, uint16_t bottom);
#endif
  // Transaction API not used by GFX
  void setAddrWindow(uint16_t x, uint16_t y, uint16_t w, uint16_t h);

  uint8_t readcommand8(uint8_t reg, uint8_t index = 0);
};

#endif // _INVECTOR_R61531H_
