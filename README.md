Arduino Driver for the R61531 LCD Driver.

This is a driver that works together with the Arduino GFX framework to support
displays using the R61531 driver chip.

It was cloned from the Arduino_ILI9341 driver and modified (kludged) to support
the new driver chip. A number of performance increases was also implemented to
make it usable in real products.

As it stands it is pretty fast when doing block transfers. Functions like
line draw suffers from poor performance due to the way data is transfered to
the chip. Maybe there is a better way to do this but I haven't found it yet.

As this was targeted to a ESP8266 design, which has no DMA capabilites on the
HSPI port, all transfers are done by the processor itself which essentially
becomes the botleneck of the system.
